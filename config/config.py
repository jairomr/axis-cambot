from configparser import ConfigParser

class Config():
    """
    Class responsavel por carregar as configuraçãoes
    """
    def __init__(self, file = None):
        self.__c = ConfigParser(allow_no_value=True)
        self.d = None
        if file != None:
            self.read(file)

    def read(self, string):
        self.__c.read(f"config/{string}.ini")
        self.d = dict(self.__c)


    def config(self, root, value):
        try:
            return self.d[root][value]
        except Exception as e:
            return None
