"""
"
"
"""

#from bs4 import BeautifulSoup
import os
import urllib.parse
import json
import datetime
import time
from requests.auth import HTTPBasicAuth
import requests
import xmltodict

#Local Libs
from config.config import Config

c = Config('config')


user = c.config('user','user')
password = c.config('user','pass')
ip = c.config('host','ip')
d_path = c.config('path','download')



class CamBot:
    def __init__(self, driver):
        self.driver = driver
        #self.driver.set_preference("browser.download.dir", d_path)
        self.url = f'http://{user}:{password}@{ip}/operator/recList.shtml?maxnumberofresults=100'
        self.close = False

        
    def open(self):
     	self.driver.get(self.url)

    def list(self):
    	base = f"http://{ip}/axis-cgi/record/list.cgi?maxnumberofresults=100"
    	ts = datetime.datetime.now().timestamp()
    	q = f"&startatresultnumber=0&recordingid=all&timestamp={ts}"

    	self.driver.get(base+q)
    	d = self.driver.find_element_by_tag_name("body").text
    	d= d.replace(
    		"This XML file does not appear to have any style information associated with it. The document tree is shown below.\n"
    		,"")
    	return json.loads(json.dumps(xmltodict.parse(d)))


    def run(self):
    	self.open()
    	files = self.list()
    	files_move = {}
    	for file in files['root']['recordings']['recording']:
    		if not os.path.isfile(f"{d_path}{file['@recordingid']}.mkv"):
    		    print("Baixado "+file['@recordingid'])
    		    u = f"http://{ip}/axis-cgi/record/export/exportrecording.cgi?"
    		    params = {
		    		'schemaversion':file['@source'],
		    		'recordingid':file['@recordingid'],
		    		'exportformat':'matroska',
		    		'diskid':file['@diskid'],
		    		'starttime':file['@starttime'],
		    		'stoptime':file['@stoptime'],
		    		'filename':file['@recordingid']
		    	}
    		    files_move[f"{file['@recordingid']}.mkv"] = False
    		    q = urllib.parse.urlencode(params)
    		    self.driver.execute_script(f'''window.open("{u}{q}","_blank");''')
    		else:
    		    print("Ja foi baixaido "+file['@recordingid'])
    	return files_move

	    	

