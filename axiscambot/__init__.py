__author__ = "Rocha, Jairo Matos"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = ["Rocha Jairo"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Rocha Jairo"
__email__ = "contato@jairomr.com.br"
__status__ = "Production"