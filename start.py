import os
import time
import shutil
from pathlib import Path
path = os.path.dirname(os.path.abspath(__file__))


os.chdir(path)



from selenium import webdriver
#Local Libs
from config.config import Config
from axiscambot.main import CamBot

c = Config('config')



d_path = c.config('path','download')

def main():
	print('Start CamBot')

	ff = webdriver.Chrome('bins/chromedriver')
	g = CamBot(ff)

	files = g.run()

	download = str(Path.home())+"\\Downloads\\"
	print(download)
	move = True
	while move:
		for x,i in enumerate(files):
			if os.path.isfile(f"{download}{i}") and files[i]==False:
				print(f"Movendo: {i} |{x+1} de {len(files)}")
				time.sleep(2)
				shutil.move(f"{download}{i}", f"{d_path}{i}")
				files[i] = True
		move = not all([files[i] for i in files])

	ff.close()

if __name__ == '__main__':
	main()