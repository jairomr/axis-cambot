""" Programa o bot para rodar em uma hora específica"""
import sched
import time
from datetime import datetime, timedelta

import start

scheduler = sched.scheduler(timefunc=time.time)



def reschedule(hour=20,minutes=0):
	new_time = datetime.now().replace(hour=0,minute=minutes,second=0, microsecond =  0)
	new_time += timedelta(hours=hour)


	if new_time < datetime.now():
		new_time += timedelta(hours=24)

	print(new_time)
	scheduler.enterabs(new_time.timestamp(),priority=0,action=cambot)




def cambot():
	start.main()
	reschedule()

scheduler.run(blocking=True)